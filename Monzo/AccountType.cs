﻿using System;

namespace Monzo
{
    public enum AccountType
    {
        uk_prepaid = 1,
        uk_retail = 2
    }
}
